Vue.createApp({
  template: "#tpl",
  data() {
    return {
      books: [
        {
          id: 1,
          name: "算法导论",
          date: "2006-09",
          price: 85,
          count: 1,
          removing: false,
        },
        {
          id: 2,
          name: "UNIX编程艺术",
          date: "2006-02",
          price: 59,
          count: 1,
          removing: false,
        },
        {
          id: 3,
          name: "编程珠玑",
          date: "2008-10",
          price: 39,
          count: 1,
          removing: false,
        },
        {
          id: 4,
          name: "代码大全",
          date: "2006-03",
          price: 128,
          count: 1,
          removing: false,
        },
      ],
    };
  },
  computed: {
    amount() {
      return this.books
        .map((b) => b.price * b.count)
        .reduce((t, p) => t + p, 0);
    },
  },
  methods: {
    incrementCount(idx, n) {
      this.books[idx].count += n;
      if (this.books[idx].count < 0) {
        this.books[idx].count = 0;
      }
    },
    remove(book) {
      this.books = this.books.filter((b) => b.id !== book.id);
    },
    setRemove(idx, remove) {
      this.books[idx].removing = remove;
    },
  },
}).mount("#app");
